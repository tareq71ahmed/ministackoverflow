    <?php

    /*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('/', function () {
    return view('welcome');
        });

    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');


     //question

    Route::resource('questions', 'QuestionController')  ->except('show');

    Route::get('questions/{slug}','QuestionController@show')->name('questions.show');

    //answer

    Route::resource('questions.answers', 'AnswerController')  ->except(['index','create','show']);


    //Accept-Answer-Controller

    Route::post('/answers/{answer}/accept', 'AcceptAnswerController@accept')->name('accept.answers');


    //Questions favorites

    Route::post('/questions/{question}/favorites', 'FavoritesController@store') -> name('questions.favorite');

    Route::delete('/questions/{question}/favorites', 'FavoritesController@destroy') -> name('questions.unfavorite');


     //questions  vote +1,-1
    Route::post('/questions/{question}/vote', 'VoteQuestionController@vote') -> name('questions.vote');


    //answers vote +1, -1

    Route::post('/answers/{answer}/vote', 'VoteAnswerController@vote') -> name('answers.vote');






