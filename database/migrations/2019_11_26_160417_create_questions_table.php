<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('questions', function (Blueprint $table) {

      $table->increments('id');
      $table->string('title');
      $table->string('slug')->unique();//unique mana holo copy hobe nah,aktar sathe arekta milbe nah
      $table->text('body');
      $table->unsignedInteger('views')->default(0);
      $table->unsignedInteger('answers')->default(0);//unsignedInteger mane jinista negative hobe nah positive hobe

      $table->integer('votes')->default(0);
      $table->unsignedInteger('best_answer_id')->nullable();//unsignedInteger mane jinista negative hobe nah positive hobe

      $table->unsignedInteger('user_id');
      $table->timestamps();

      $table->foreign('user_id')->references('id')->
      on('users')->onDelete('cascade');


    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('questions');
  }
}
