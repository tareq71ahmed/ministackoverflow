<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('answers', function (Blueprint $table) {


      $table->increments('id');


      $table->unsignedInteger('question_id');  //unsignedInteger mane jinista negative hobe nah positive hobe
      $table->unsignedInteger('user_id');
      $table->text('body');
      $table->integer('votes_count')->default(0);


      $table->timestamps();


      $table->foreign('user_id')->references('id')->
      on('users')->onDelete('cascade');      //amar kora,ata kora chilo nah,atar bodele AnswerFactory.php te
//           'user_id'=>App\User::pluck('id')->random(), chilo  akn ata korchi ar 'user_id'=>App\User::pluck('id')->random(),
//            ar bodele   AnswerFactory.php te 'user_id'=>rand(1,5), dici






    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('answers');
  }
}
