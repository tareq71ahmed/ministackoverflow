<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{


  // Mass Assignment
  protected $fillable= ['title','body'];


  // One to many(inverse)
  public  function user(){

    return $this->belongsTo('App\User');
  }

    // slug
  public function setTitleAttribute($value)
  {
    $this->attributes['title'] = $value;
    $this->attributes['slug'] = str_slug($value);
  }


    //Route Binding

  public function getUrlAttribute($value){
    return route('questions.show',$this->slug);
  }




  //date Eloqurent

  public function getCreateDateAttribute($value)
  {
    return $this->created_at->diffForhumans() ;
  }

   //unanswered,answered,answered-accepted

  public  function  getStatusAttribute(){

    if($this->answers_count > 0){

      if($this->best_answer_id){

        return 'answered-accepted';

      }
      return 'answered';


    }
    return 'unanswered' ;




  }



  // question body parse //purifier used clean( )
  public function getBodyHtmlAttribute(){

    return clean(\Parsedown::instance()->text($this->body));

  }






    // One To Many
  public function answers(){
    return $this->hasMany(Answer::class);
  }


   // best answer select

    public function bestAcceptAnswer(Answer $answer){
        $this->best_answer_id = $answer->id;
        $this->save();


    }




    //Many to many

    public function favorites(){
        return $this->belongsToMany('App\User','favorites')->withTimestamps();
    }






//         Eloquent accessor
//          method

    public function isFavorited(){
        return $this->favorites()->where('user_id',auth()->id())->count() > 0;
    }

    //method ta ke attribute a convert korbo

    public  function getIsFavoritedAttribute(){
        return $this->isFavorited();

    }

    // arekta attribute toiri korlam

    public  function getFavoritesCountAttribute(){

        return $this->favorites()->count();

    }


    //Many To Many Polymorphic Relations, topic question and answer vote

    public function votes(){
        return $this->morphToMany(User::class, 'votable')
            -> withTimestamps();
    }


    public function upVotes(){
        return $this->votes()->wherePivot('vote', 1);
    }
    public function downVotes(){
        return $this->votes()->wherePivot('vote', -1);
    }





//$appends ar kaj holo vue te jaigula undefined oigula pauano,jaigula elloqurent oigula pauano
    protected $appends=['create_date'];




}
