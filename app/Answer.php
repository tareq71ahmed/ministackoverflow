<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;

class Answer extends Model
{


  // Mass Assignment

 //   protected $fillable= ['body','user->id'];




  //One To Many (Inverse)

  public  function user(){

    return $this->belongsTo('App\User');
  }


  //One To Many (Inverse)

  public function question(){

    return $this->belongsTo('App\Question');

  }


  // question body parse akhane disi
  public function getBodyHtmlAttribute(){

    return clean(\Parsedown::instance()->text($this->body));

  }


  // Answer increment , over right

  public  static function boot()
  {
    parent::boot();
    static::created(function ($answer) {
      $answer->question->increment('answers_count');

    });


    static::deleted(function ($answer){
//       $answer->question->decrement('answers_count');

      //  kau best answer delete kore dile  onno arekta select korte parbo,atar jonna


        $question = $answer->question;
        $question->decrement('answers_count');
        if($question->best_answer_id === $answer->id){
            $question-> best_answer_id = NULL;
            $question->save();
        }


    });


  }




  //date Eloqurent

  public function getCreateDateAttribute($value)
  {
    return $this->created_at->diffForhumans() ;
  }




        //best answer select
      public function  getStatusAttribute(){

          return $this->id===$this->question->best_answer_id ? 'vote-accept' : '';

      }

      // best answer dekte parbo,jokn j question korce o best answer select korce
    public function getIsBestAttribute(){
        return $this->id === $this->question->best_answer_id ;
    }



    //Many To Many Polymorphic Relations, topic question and answer vote

    public function votes(){
        return $this->morphToMany(User::class,'votable')->withTimestamps();
    }



    public function  upVotes(){
        return $this->votes()->wherePivot('vote',1);
    }


    public function  downVotes(){
        return $this->votes()->wherePivot('vote',-1);
    }





//$appends ar kaj holo vue te jaigula undefined oigula pauano,jaigula elloqurent oigula pauano
    protected $appends=['create_date','body_html'];








}
