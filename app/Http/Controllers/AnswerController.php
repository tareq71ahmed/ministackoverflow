<?php

namespace App\Http\Controllers;


use App\Answer;
use Illuminate\Http\Request;
use App\Question;
use App\Http\Requests\AnswerRequest;
use Illuminate\Support\Facades\Auth;
use App\User;



class AnswerController extends Controller
{



  public function __construct(){
    return $this->middleware('CheckRole');
  }




  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(AnswerRequest $request,Question $question)
  {

//        $question->answers()->create($request->validate(['body'=>'required'])
//            +['user_id'=>\Auth::id()]);			//answers holo model a method
//
//        return back()->with('success','Answer submitted');




    $questions= new Answer();
    $questions->user_id=Auth::id();
    $questions->question_id=$question->id;
    $questions->body=$request['body'];
    $questions->save();
    return back()->with('Your Answer submitted Successfully');





//
//        $answer= new Answer();
//        $answer->user_id=Auth::id();
//        $answer->question_id=$question->id;
//        $answer->body=$request['body'];
//        $answer->save();
//        return back()->with('Your Answer submitted Successfully');








  }



  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Answer  $answer
  * @return \Illuminate\Http\Response
  */

  public function edit(Question $question, Answer $answer)
  {

    $this->authorize('update',$answer);

    $question->user_id=Auth::id();
    $answer->user_id=Auth::id();
    $question->question_id=$question->id;

    return view('answers.edit',compact('question','answer'));


//        $question->question_id=$question->id;
//        $question->user_id=Auth::id();
//        $answer->delete();
//        return back()->with('success',' Answer successfully deleted');



  }



  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Answer  $answer
  * @return \Illuminate\Http\Response
  */



  public function update(AnswerRequest $request,Question $question, Answer $answer)
  {


    $this->authorize('update', $answer);


    $question->user_id=Auth::id();
    $answer->user_id=Auth::id();
    $question->question_id=$question->id;

    $answer->body=$request['body'];
    $answer->save();

      if($request->expectsJson()){
          return response()->json([
              'message'=> 'Answer updated Successfully',
              'body_html' => $answer->body_html
          ]);
      }


    return redirect()-> route('questions.show',$question->slug)->with('success',' Answer Updated');


  }



  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Answer  $answer
  * @return \Illuminate\Http\Response
  */



  public function destroy(Question $question ,Answer $answer)
  {



//        $this->authorize('delete',$answer);
//        $answer->delete();
//        return back()->with('success','Delete Successfully') ;
//


    $this->authorize('delete', $answer);
    $question->user_id=Auth::id();
    $answer->user_id=Auth::id();
    $question->question_id=$question->id;
    $answer->delete();


      if(request()->expectsJson()){
          return response()->json([
              'message'=> 'Answer Delete SUCCESSFULLY'
          ]);
      }


    return back()->with('success','Your Answer Deleted');
  }




}
