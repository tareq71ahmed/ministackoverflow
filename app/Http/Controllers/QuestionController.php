<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use App\Http\Requests\QuestionRequest;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\CheckRole;


class QuestionController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */




  /**
  1. php artisan make:middleware CheckRole

   2.CheckRole.php te
   use Illuminate\Support\Facades\Auth

   public function handle($request, Closure $next)
  {

  if (Auth::check() == false ){
  return redirect('/login');
  }

  return $next($request);
  }

  3. kernel.php te

  'CheckRole'=>\App\Http\Middleware\CheckRole::class

  */


  public function __construct(){
    return $this->middleware('CheckRole',['except'=>['index','show']]);
  }


//   public function __construct(){
//       return $this->middleware('auth',['except'=>['index','show']]);
//   }



  public function index()
  {
    $questions = Question::latest()->paginate(5);
    return view('questions.index',compact('questions'));

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $questions = new Question();
    $questions->user_id=Auth::id();
    return view('questions.create',compact('questions'));


  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(QuestionRequest $request)
  {


    $questions = new Question();
    $questions->user_id=Auth::id();
    $questions->title=$request['title'];
    $questions->body=$request['body'];
    $questions->save();

    return redirect()->route('questions.index')->
    with('success','Question submitted Successfully');




  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Question  $question
  * @return \Illuminate\Http\Response
  */
  public function show(Question $question)
  {
    return view('questions.show',compact('question'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Question  $question
  * @return \Illuminate\Http\Response
  */

  public function edit(Question $question)
  {


//        if(\Gate::denies('update-question', $question)){
//            return abort(403, 'Access Denied');
//        }

    $this->authorize('update', $question);       //ata likbo,gate kete


    $question->user_id=Auth::id();
      return view('questions.edit',compact('question'));
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Question  $question
  * @return \Illuminate\Http\Response
  */

  public function update(QuestionRequest $request, Question $question)
  {

//        if(\Gate::denies('update-question', $question)){
//            return abort(403, 'Access Denied');
//        }


    $this->authorize('update', $question);       //ata likbo,gate kete


    $question->user_id=Auth::id();
    $question->title=$request['title'];
    $question->body=$request['body'];
    $question->save();

    return redirect()->route('questions.index')->
    with('success','Question Updated Successfully');

  }


  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Question  $question
  * @return \Illuminate\Http\Response
  */

  public function destroy(Question $question)
  {

//        if(\Gate::denies('delete-question', $question)){
//            return abort(403, 'Access Denied');
//        }

    $this->authorize('delete', $question);       //ata likbo,gate kete


    $question->user_id=Auth::id();
    $question->delete();
    return redirect()->route('questions.index')->
    with('success','Question Deleted Successfully');




  }


}
