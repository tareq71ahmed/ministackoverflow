

@extends('layouts.app')

@section('content')

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12 ">
        <div class="card ">
          <div class="card-header">

            <div class="float-left font-weight-bold"> <h3> Edit Answer</h3> </div>


            <div class="d-flex float-right">
              <a href="{{route('questions.show',$question->slug)}}" class="btn btn-outline-success">Back</a>

            </div>

          </div>


          <div class="card-body">
            <div class="media">

              <div class="media-body">


                @if($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif

                @if(Session::has('success'))
                  <div class="alert alert-success">
                    {{Session::get('success')}}
                  </div>
                @endif


   {{--    action="{{route('questions.answers.update',[$question->id,$answer->id])}}--}}


                <form    action="{{route('questions.answers.update',[$question->id,$answer->id])}}"  method="post">
                  @method('PUT')
                  @csrf


                  <div class="form-group">
                    <label class="control-label">Update answer</label>

                    <textarea  name="body" class="form-control" rows="7" placeholder="Enter your Quetion">{{$answer->body}}</textarea>



                  </div>



                  <button type="submit" class="btn btn-outline-success">Update & save your Answer</button>


                </form>

              </div>

            </div>

          </div>




        </div>
      </div>
    </div>
  </div>

@endsection
