


@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h3>Your Questions</h3>
            </div>


            <diV class="float-right mx-auto">

              <a href="{{route('questions.index')}}" class="btn btn-outline-success">Back to All Question</a>


            </diV>

          </div>

            <div class="card-body">
              <div class="media">
                <div class="media-body">



                  <form action="{{route('questions.store')}}"  method="POST" >
                    @csrf

                      @method("post")

                    <div class="form-group ">
                      <label for="title" >Question Title</label>

                        <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                        @if ($errors->has('title'))
                          <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('title') }}</strong>
                  </span>
                        @endif

                    </div>

                    <div class="form-group ">
                      <label for="body" >Question Body</label>

                      <textarea id="body"  type="text" rows="12" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" value="{{ old('body') }}" required></textarea>


                        @if ($errors->has('body'))
                          <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('body') }}</strong>
                  </span>
                        @endif


                    </div>

                        <button type="submit" class="btn btn-outline-success">Submit </button>

                  </form>






              </div>
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
@endsection
