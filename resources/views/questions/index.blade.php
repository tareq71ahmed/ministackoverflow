
@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="float-left">
              <h3>All Questions</h3>
            </div>


            <diV class="float-right mx-auto">

              <a href="{{route('questions.create')}}" class="btn btn-outline-success">Ask Question</a>


            </diV>

          </div>


          @include('layouts._message')


          @foreach($questions as $question)
            <div class="card-body">
              <div class="media">


                <div class="d-flex flex-column counters">

                 <div class="vote">
                   <strong>
                     {{$question->votes_count}}
                     {{ str_plural('vote') }}
                   </strong>
                 </div>

                  <div class="status {{$question->status}}">
                    <strong>
                      {{$question->answers_count}}
                      {{ str_plural('Answer') }}
                    </strong>
                  </div>

                  <div class="view">
                    <strong>
                      {{$question->views}}
                      {{ str_plural('view') }}
                    </strong>
                  </div>

                </div>




                <div class="media-body">
                  <h3 class="mt-0">
                    <a href="{{$question->url}}">
                      {{$question->title}}
                    </a>
                    <p class="lead">
                      Asked By <a href="{{$question->user->url}}">
                        {{$question->user->name}}
                      </a>
                      <small class="text-muted">
                        {{$question->create_date}}
                      </small>
                    </p>
                  </h3>
                  {{str_limit(strip_tags($question->body,350))}}
                </div>


                <div class="d-flex flex-column">


            {{--  @if(Auth::user()->can('update-question', $question))--}}

                  @can('update',$question)




                  <a href="{{route('questions.edit',$question->id)}}" class="btn btn-outline-success btn-sm mb-3">Edit</a>



                {{--     @endif--}}
                {{--    @if(Auth::user()->can('delete-question', $question))--}}

                  @endcan

                  @can('delete',$question)




                  <form class="form-delete d-inline" action="{{route('questions.destroy',$question->id)}}" method="post">
                    @method('DELETE')
                    @csrf
                    <button onclick="return confirm('Warning! Are You Sure want to delete? ')" class="btn btn-outline-danger btn-sm  " type="submit">Delete</button>
                  </form>
                {{-- @endif--}}

                  @endcan


                </div>





              </div>
            </div>
            <hr>
          @endforeach

        <!-- <div class="mx-auto">
{{--                {{$questions->links()}}--}}
          </div>-->
{{--                   {{$questions->links()}}--}}


          {{$questions->render()}}


        </div>
      </div>
    </div>
  </div>
@endsection
