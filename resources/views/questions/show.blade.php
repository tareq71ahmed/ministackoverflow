
@extends('layouts.app')

@section('content')
<div class="container">
 <div class="row justify-content-center">
 <div class="col-md-12">
  <div class="card">
  <div class="card-body">

   <div class="card-title">
   <div class="d-flex align-items-center">
    <h3>{{$question->title}}</h3>
    <h3 class="ml-auto">
    <a class="btn btn-outline-secondary" href="{{route('questions.index')}}">Back To All Question</a>

    </h3>
   </div>
   </div>
   <hr>
   <div class="media">

   {{-- question  votes-part  --}}

    <div class="d-flex flex-column votes-control">
        <a class="vote-up
    {{Auth::guest() ? 'off' : ''}}" onclick="event.preventDefault(); document.getElementById('questions-vote-up{{$question->id}}').submit()">
            <i  class=" fa fa-caret-up fa-3x"></i>
        </a>
        <span class="votes-count">
                                    {{$question->votes_count}}
                                </span>

        <form action="/questions/{{$question->id}}/vote " id="questions-vote-up{{$question->id}}" style="display: none" method="post">
            @csrf
            <input type="hidden" name="vote" value="1">



        </form>

{{--        <span class="votes-count">1205</span>--}}

        <a class="vote-down
                        {{Auth::guest() ? 'off' : ''}}" onclick="event.preventDefault(); document.getElementById('questions-vote-down{{$question->id}}').submit()">

            <i class=" fa fa-caret-down fa-3x"></i>
        </a>

        <form action="/questions/{{$question->id}}/vote " id="questions-vote-down{{$question->id}}" style="display: none" method="post">
            @csrf
            <input type="hidden" name="vote" value="-1">



        </form>

        <a  onclick="event.preventDefault(); document.getElementById('questions-favorites-{{$question->id}}').submit()"
            class="favourite mt-3
        {{Auth::guest() ? 'off' :($question->is_favorited) ? 'fabs': '' }}  ">
            <i class=" fas fa-star fa-2x"></i>

            <span class="faboritted">
                                    {{$question->favorites_count}}

                                </span>

        </a>

        <form action="/questions/{{$question->id}}/favorites " id="questions-favorites-{{$question->id}}" style="display: none" method="post">
            @csrf

            @if($question->is_favorited)

                @method('DELETE')
            @endif

        </form>


    </div>

    {{-- question  votes-part end --}}


    <div class="media-body">

    {!! $question->body_html !!}
    <div class="float-right">

        {{--   vue  --}}

        <user-info label="Asked" :model="{{$question}}" ></user-info>

        {{--   vue  end--}}


    </div>
   </div>
   </div>
  </div>
  </div>
 </div>
 </div>






 {{--------------------------- <answer part> ---------------------}}


 <div class="row mt-5" v-cloak>

 <div class="col-md-12">
  <div class="card">
  <div class="card-body">
   <div class="card-title">
       <h3>

    <h2>Your Answer</h2>
    @include('layouts._message')

    <form action="{{route('questions.answers.store',$question->id)}}" method="post">
     @csrf
     <div class="form-group">
      <textarea rows="7" class="form-control {{ $errors->has('body') ? ' is-invalid' : '' }}" name="body"></textarea>


      @if ($errors->has('body'))
       <span class="invalid-feedback" role="alert">
     <strong>{{ $errors->first('body') }}</strong>
       </span>
      @endif

     </div>
     <div class="form-group">
      <button class="btn btn-outline-success  " type="submit">SUBMIT</button>
     </div>

    </form>

    {{$question->answers_count}} {{ str_plural('Answer') }}
       </h3>

   <hr>
   @foreach($question->answers as $answer)
    <div class="media">


     {{-- answers  votes-part  --}}

     <div  class="d-flex flex-column votes-control">
         <a class="vote-up
                               {{Auth::guest() ? 'off' : ''}}"

            onclick="event.preventDefault(); document.getElementById('answers-vote-up{{$answer->id}}').submit()">
             <i class="fas fa-caret-up fa-3x"></i>
         </a>
         <span class="votes-count">
                                {{$answer->votes_count}}
                            </span>
         <form action="/answers/{{$answer->id}}/vote" id="answers-vote-up{{$answer->id}}"
               style="display: none" method="post">
             @csrf
             <input type="hidden" name="vote" value="1">
         </form>

{{--         <span class="votes-count">1205</span>--}}

         <a class="vote-down
                               {{Auth::guest() ? 'off' : '' }}"
            onclick="event.preventDefault(); document.getElementById('answers-vote-down{{$answer->id}}').submit()">
             <i class="fas fa-caret-down fa-3x"></i>
         </a>
         <form action="/answers/{{$answer->id}}/vote" id="answers-vote-down{{$answer->id}}"
               style="display: none" method="post">
             @csrf
             <input type="hidden" name="vote" value="-1">
         </form>

         {{-- best answer select --}}

         @can('accept',$answer)

      <a class="favourite mt-3  {{$answer->status}}"

         onclick="event.preventDefault(); document.getElementById('accept-answer-{{$answer->id}}').submit()">
       <i class=" fas fa-check fa-2x"></i>

       {{-- <span class="faboritted">1234</span>--}}

      </a>
          <form action="{{route('accept.answers',$answer->id)}}" id="accept-answer-{{$answer->id}}" style="display: none" method="post">
              @csrf

          </form>

         @else
             @if($answer->is_best)

                 <a class="favourite mt-3 {{$answer->status}}">

                 <i class=" fas fa-check fa-2x"></i>
                 </a>

             @endif


             @endcan

     </div>

     {{-- answers  votes-part end  --}}

        {{-- .............................................................. --}}

        <answer :answer="{{$answer}}" inline-template>

       <div class="media-body">

                {{--    {!! $answer->body_html !!}  --}}    {{--  parse korci--}}



           <form v-if="editing" @submit.prevent="update">

       <div class="form-group">

        <textarea required v-model="body" rows="10" class="form-control" ></textarea>       {{-- $answer->answer->body--}}

       </div>

       <button :disabled="isInvalid"  class="btn btn-outline-success" >update</button>
       <button  class="btn btn-outline-secondary" @click="cancel" >Cancel</button>

           </form>


     <div v-else>

         <div v-html="bodyHtml"></div>


     <div class="row">



      <div class="col-md-4">

       <div class="ml-auto">       {{-- d-flex flex-column float-left--}}
         @can('update',$answer)

               <a @click.prevent="edit" class="btn btn-outline-success btn-sm mb-3 d-inline">Edit</a>

               {{--href="{{route('questions.answers.edit',[$question->id,$answer->id])}}"--}}


           @endcan
         @can('delete',$answer)

{{--         <form class="form-delete d-inline" action="{{route('questions.answers.destroy',[$question->id,$answer->id])}}"  method="post">--}}
{{--          @method('delete')--}}
{{--          @csrf--}}
{{--          <button onclick="return confirm('Warning! Are You Sure want to delete? ')" class="btn btn-outline-danger btn-sm  " type="submit">Delete</button>--}}
{{--         </form>--}}

             <button @click="destroy" class="btn btn-sm btn-outline-danger">Delete</button>


         @endcan

       </div>



      </div>


       <div class="col-md-4">

       {{--   empty  --}}

      </div>


          {{--  Answer ->user name,answered date &avatar--}}

      <div class="col-md-4">
       <div class="float-right">

           {{--   vue  --}}
           <user-inf label="" :model="{{$answer}}" ></user-inf>

           {{--   vue  end--}}

       </div>
      </div>
       {{--  Answer ->user name,answered date &avatar End--}}


     </div>

     </div>

    </div>

        </answer>

{{--   .............................................................. --}}


    </div>
    <hr>

   @endforeach
   </div>

  </div>
  </div>

 </div>
 </div>

</div>


@endsection
